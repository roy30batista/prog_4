from flask import Flask, render_template, request, redirect, url_for
import redis

app = Flask(__name__)
db = redis.Redis(host='localhost', port=6379, db=0)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/agregar', methods=['GET', 'POST'])
def agregar_palabra():
    if request.method == 'POST':
        palabra = request.form['palabra']
        significado = request.form['significado']
        id_palabra = db.incr('id_palabra')
        db.hset(f'palabra:{id_palabra}', 'palabra', palabra)
        db.hset(f'palabra:{id_palabra}', 'significado', significado)
        return redirect(url_for('listar_palabras'))
    else:
        return render_template('agregar_palabras.html')

@app.route('/listar_palabras')
def listar_palabras():
    palabras = []
    lista_palabras = db.keys('palabra:*')
    for key in lista_palabras:
        palabra = db.hgetall(key)
        palabra_id = key.decode('utf-8').split(':')[1]
        palabras.append((palabra_id, palabra[b'palabra'].decode('utf-8'), palabra[b'significado'].decode('utf-8')))
    palabras_ordenadas = sorted(palabras, key=lambda x: x[0])
    return render_template('listar_palabras.html', palabras=palabras_ordenadas)

@app.route('/editar_palabra/<int:id_palabra>', methods=['GET', 'POST'])
def editar_palabra(id_palabra):
    if request.method == 'GET':
        palabra = db.hgetall(f'palabra:{id_palabra}')
        palabra = (id_palabra, palabra[b'palabra'].decode('utf-8'), palabra[b'significado'].decode('utf-8'))
        return render_template('editar_palabra.html', palabra=palabra)
    elif request.method == 'POST':
        palabra = request.form['palabra']
        significado = request.form['significado']
        db.hset(f'palabra:{id_palabra}', 'palabra', palabra)
        db.hset(f'palabra:{id_palabra}', 'significado', significado)
        return redirect(url_for('listar_palabras'))


if __name__ == '__main__':
    app.run(debug=True)
