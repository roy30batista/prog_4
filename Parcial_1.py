import sys

class Inventario:
    def __init__(self):
        self.articulos = []

    def registrar_articulo(self, codigo, nombre, cantidad, precio):
        self.articulos.append({'codigo': codigo, 'nombre': nombre, 'cantidad': cantidad, 'precio': precio})
        print('Artículo registrado con éxito')

    def listar_articulos(self):
        if len(self.articulos) == 0:
            print("\nNo hay artículos registrados.")
        else:
            for articulo in self.articulos:
                print(f"Código: {articulo['codigo']}\nNombre: {articulo['nombre']}\nCantidad: {articulo['cantidad']}\nPrecio: {articulo['precio']}\n")


    def buscar_articulo(self, codigo):
        for articulo in self.articulos:
            if articulo['codigo'] == codigo:
                print(f"Código: {articulo['codigo']}\nNombre: {articulo['nombre']}\nCantidad: {articulo['cantidad']}\nPrecio: {articulo['precio']}")
                return articulo
        print(f"No se encontró ningún artículo con el código {codigo}")
        return None

    def editar_articulo(self, codigo, nombre, cantidad, precio):
        articulo = self.buscar_articulo(codigo)
        if articulo:
            articulo['nombre'] = nombre
            articulo['cantidad'] = cantidad
            articulo['precio'] = precio
            print('Artículo editado con éxito')

    def eliminar_articulo(self, codigo):
        articulo = self.buscar_articulo(codigo)
        if articulo:
            self.articulos.remove(articulo)
            print('Artículo eliminado con éxito')

def menu():
    print("\nSistema de inventario")
    print("1. Registrar artículo")
    print("2. Listar articulo")
    print("3. Buscar artículo")
    print("4. Editar artículo")
    print("5. Eliminar artículo")
    print("6. Salir")
    return int(input("Seleccione una opción: "))

inventario = Inventario()

while True:
    opcion = menu()

    if opcion == 1:
        codigo = input("\nCódigo del artículo: ")
        nombre = input("Nombre del artículo: ")
        cantidad = int(input("Cantidad del artículo: "))
        precio = float(input("Precio del artículo: "))
        inventario.registrar_articulo(codigo, nombre, cantidad, precio)

    if opcion == 2:
        inventario.listar_articulos()

    elif opcion == 3:
        codigo = input("\nCódigo del artículo a buscar: ")
        inventario.buscar_articulo(codigo)

    elif opcion == 4:
        codigo = input("\nCódigo del artículo a editar: ")
        nombre = input("Nuevo nombre del artículo: ")
        cantidad = int(input("Nueva cantidad del artículo: "))
        precio = float(input("Nuevo precio del artículo: "))
        inventario.editar_articulo(codigo, nombre, cantidad, precio)

    elif opcion == 5:
        codigo = input("\nCódigo del artículo a eliminar: ")
        inventario.eliminar_articulo(codigo)

    elif opcion == 6:
        print('\nHasta luego.')
        break

    else:
        print('\nOpción inválida')
