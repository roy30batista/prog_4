#Instalacion de Redis
#sudo apt-get update
#sudo apt-get install redis-server
#activar o correr el redis 
#sudo service redis-server start
#redis-cli ping
#la respuesta de esto debe ser PONG para saber que está activo.


import redis

# Conexión a Redis
db = redis.Redis(host='localhost', port=6379, db=0)

while True:
    print('Seleccione una opción:')
    print('1) Agregar nueva palabra')
    print('2) Editar palabra existente')
    print('3) Eliminar palabra existente')
    print('4) Ver listado de palabras')
    print('5) Buscar significado de palabra')
    print('6) Salir')

    opcion = input('> ')

    try:
        opcion = int(opcion)

        if opcion == 1:
            palabra = input('Ingrese la palabra: ')
            significado = input('Ingrese el significado: ')

            id_palabra = db.incr('id_palabra')
            db.hset(f'palabra:{id_palabra}', 'palabra', palabra)
            db.hset(f'palabra:{id_palabra}', 'significado', significado)

            print(f'La palabra ha sido agregada exitosamente con ID {id_palabra}.')

        elif opcion == 2:
            id_palabra_editar = input('Ingrese el ID de la palabra a editar: ')
            palabra_nueva = input('Ingrese la nueva palabra: ')
            significado_nuevo = input('Ingrese el nuevo significado: ')

            palabra_editar = db.hgetall(f'palabra:{id_palabra_editar}')
            palabra_editar[b'palabra'] = palabra_nueva.encode('utf-8')
            palabra_editar[b'significado'] = significado_nuevo.encode('utf-8')
            db.hmset(f'palabra:{id_palabra_editar}', palabra_editar)

            print('La palabra ha sido editada exitosamente.')

        elif opcion == 3:
            id_palabra_eliminar = input('Ingrese el ID de la palabra a eliminar: ')
            db.delete(f'palabra:{id_palabra_eliminar}')

            print('La palabra ha sido eliminada exitosamente.')

        elif opcion == 4:
            lista_palabras = db.keys('palabra:*')

            print('Listado de palabras:')
            for key in lista_palabras:
                palabra = db.hgetall(key)
                print(f'{key.decode("utf-8").split(":")[1]} - {palabra[b"palabra"].decode("utf-8")}: {palabra[b"significado"].decode("utf-8")}')

        elif opcion == 5:
            palabra_buscar = input('Ingrese la palabra a buscar: ')

            lista_palabras = db.keys('palabra:*')
            resultados = []

            for key in lista_palabras:
                palabra = db.hgetall(key)
                if palabra[b'palabra'].decode('utf-8').lower().find(palabra_buscar.lower()) != -1:
                    resultados.append(palabra)

            if len(resultados) > 0:
                print('Resultados de la búsqueda:')
                for palabra in resultados:
                    print(f'{palabra[b"palabra"].decode("utf-8")}: {palabra[b"significado"].decode("utf-8")}')
            else:
                print('No se encontraron resultados para la búsqueda.')

        elif opcion == 6:
            print('Hasta luego.')
            break

        else:
            print('Opción inválida. Por favor, seleccione una opción válida.')

    except ValueError:
        print('Opción inválida. Por favor, seleccione una opción válida.')
