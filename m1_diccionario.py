import sqlite3

# Crear conexión a la base de datos
conexion = sqlite3.connect('diccionario_m1.db')

# Crear tabla de palabras si no existe
cursor = conexion.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS palabras (id INTEGER PRIMARY KEY, palabra TEXT, significado TEXT)''')
conexion.commit()

# Menú principal
while True:
    print('Seleccione una opción:')
    print('1. Agregar nueva palabra')
    print('2. Editar palabra existente')
    print('3. Eliminar palabra existente')
    print('4. Ver listado de palabras')
    print('5. Buscar significado de palabra')
    print('6. Salir')

    opcion = input('Opcion #: ')

    try:
        opcion = int(opcion)
    except ValueError:
        print('Opción inválida. Por favor ingrese un número.')
        continue

    if opcion == 1:
        palabra = input('Ingrese la palabra: ')
        significado = input('Ingrese el significado: ')
        cursor.execute('INSERT INTO palabras (palabra, significado) VALUES (?, ?)', (palabra, significado))
        conexion.commit()
        print('La palabra ha sido agregada exitosamente.')
    elif opcion == 2:
        id_palabra = input('Ingrese el ID de la palabra a editar: ')
        palabra = input('Ingrese la nueva palabra: ')
        significado = input('Ingrese el nuevo significado: ')
        cursor.execute('UPDATE palabras SET palabra = ?, significado = ? WHERE id = ?', (palabra, significado, id_palabra))
        conexion.commit()
        print('La palabra ha sido editada exitosamente.')
    elif opcion == 3:
        id_palabra = input('Ingrese el ID de la palabra a eliminar: ')
        cursor.execute('DELETE FROM palabras WHERE id = ?', (id_palabra,))
        conexion.commit()
        print('La palabra ha sido eliminada exitosamente.')
    elif opcion == 4:
        cursor.execute('SELECT * FROM palabras')
        palabras = cursor.fetchall()
        print('Listado de palabras:')
        for palabra in palabras:
            print(palabra)
    elif opcion == 5:
        palabra = input('Ingrese la palabra a buscar: ')
        cursor.execute('SELECT * FROM palabras WHERE palabra LIKE ?', ('%' + palabra + '%',))
        palabras = cursor.fetchall()
        if len(palabras) == 0:
            print('La palabra no se encontró en el diccionario.')
        else:
            for palabra in palabras:
                print(palabra)
    elif opcion == 6:
        break
    else:
        print('Opción inválida. Por favor ingrese un número del 1 al 6.')

# Cerrar conexión a la base de datos
conexion.close()
