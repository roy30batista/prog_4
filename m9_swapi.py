#si pasa error deben instalar
#pip install requests

import requests

# a) ¿En cuántas películas aparecen planetas cuyo clima sea árido?
url = 'https://swapi.dev/api/planets/'
params = {'search': 'arid'}
response = requests.get(url, params=params)

if response.status_code == 200:
    planets = response.json()['results']
    films_count = sum([len(planet['films']) for planet in planets])
    print(f"En {films_count} películas aparecen planetas cuyo clima es árido.")
else:
    print("Ha ocurrido un error al obtener la información de la API.")


# b) ¿Cuántos Wookies aparecen en la sexta película?
url = 'https://swapi.dev/api/people/'
params = {'search': 'wookiee'}
response = requests.get(url, params=params)

if response.status_code == 200:
    wookies = response.json()['results']
    episode_vi_wookies = [wookiee for wookiee in wookies if 'episode_vi' in wookiee['films'][0]]
    print(f"En la sexta película aparecen {len(episode_vi_wookies)} Wookies.")
else:
    print("Ha ocurrido un error al obtener la información de la API.")


# c) ¿Cuál es el nombre de la aeronave más grande en toda la saga?
url = 'https://swapi.dev/api/starships/'
response = requests.get(url)

if response.status_code == 200:
    starships = response.json()['results']
    largest_starship = max(starships, key=lambda s: float(s['length'].replace(',', '')))
    print(f"La aeronave más grande en toda la saga se llama {largest_starship['name']}.")
else:
    print("Ha ocurrido un error al obtener la información de la API.")
