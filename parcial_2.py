from flask import Flask, render_template
import xml.etree.ElementTree as ET
import os

app = Flask(__name__)

@app.route("/")
def index():
    tree = ET.parse(os.path.abspath("vacuna_data.xml"))
    root = tree.getroot()
    pan_records = []
    for record in root.iter('record'):
        for field in record.findall('field'):
            if field.get('key') == 'PAN':
                pan_records.append(record)
                break
    return render_template('index.html', pan_records=pan_records)

if __name__ == "__main__":
    app.run(debug=True)
