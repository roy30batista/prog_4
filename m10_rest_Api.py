from flask import Flask, request, jsonify
from flask_pymongo import pymongo

app = Flask(__name__)

# Conexión a la base de datos
client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["mydatabase"]
palabras = db["palabras"]

# Función para la página de inicio
@app.route('/')
def home():
    """
    Página de inicio.

    Returns:
        str: Mensaje de bienvenida.
    """
    return '¡Bienvenido a la página de inicio!'

# Función para obtener todas las palabras
@app.route("/palabras", methods=["GET"])
def get_palabras():
    """
    Obtiene todas las palabras y sus significados.

    Returns:
        dict: Lista de diccionarios con los campos 'id', 'palabra' y 'significado'.
    """
    result = []
    for palabra in palabras.find():
        result.append({"id": str(palabra["_id"]), "palabra": palabra["palabra"], "significado": palabra["significado"]})
    return jsonify(result)

# Función para añadir una nueva palabra
@app.route("/palabras", methods=["POST"])
def post_palabra():
    """
    Añade una nueva palabra con su significado.

    Returns:
        dict: Diccionario con los campos 'id', 'palabra' y 'significado' de la nueva palabra.
    """
    palabra = request.json["palabra"]
    significado = request.json["significado"]
    result = palabras.insert_one({"palabra": palabra, "significado": significado})
    return jsonify({"id": str(result.inserted_id), "palabra": palabra, "significado": significado})

# Función para actualizar una palabra existente
@app.route("/palabras/<id>", methods=["PUT"])
def put_palabra(id):
    """
    Actualiza la palabra y/o su significado.

    Args:
        id (str): ID de la palabra a actualizar.

    Returns:
        dict: Diccionario con los campos 'id', 'palabra' y 'significado' actualizados.
    """
    palabra = request.json["palabra"]
    significado = request.json["significado"]
    palabras.update_one({"_id": pymongo.ObjectId(id)}, {"$set": {"palabra": palabra, "significado": significado}})
    return jsonify({"id": id, "palabra": palabra, "significado": significado})

# Función para eliminar una palabra
@app.route("/palabras/<id>", methods=["DELETE"])
def delete_palabra(id):
    """
    Elimina la palabra y su significado.

    Args:
        id (str): ID de la palabra a eliminar.

    Returns:
        dict: Diccionario con el campo 'id' de la palabra eliminada.
    """
    palabras.delete_one({"_id": pymongo.ObjectId(id)})
    return jsonify({"id": id})

if __name__ == '__main__':
    # Ejecución del servidor de Flask
    app.run(debug=True, host='0.0.0.0', port=5000)
